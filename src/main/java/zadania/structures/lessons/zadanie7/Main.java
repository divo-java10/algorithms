package zadania.structures.lessons.zadanie7;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        List<Integer> linkedList = new ArrayList<>();
        Random random = new Random();
        long start = new Date().getTime();
        for (int i = 0; i < 1000000; i++) {
            linkedList.add(random.nextInt());
        }
        System.out.println("Dodaj na końcu: " + (new Date().getTime() - start) + "ms");

        linkedList = new ArrayList<>();
        start = new Date().getTime();
        for (int i = 0; i < 1000000; i++) {
            linkedList.add(0, random.nextInt());
        }
        System.out.println("Dodaj na początku: " + (new Date().getTime() - start) + "ms");
    }
}
