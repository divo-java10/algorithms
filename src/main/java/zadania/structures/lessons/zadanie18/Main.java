package zadania.structures.lessons.zadanie18;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        List<Author> authors = new Biblioteka().getAuthors();
        System.out.println(authors);
        for (Author a : authors) {
            System.out.print("(" + a.getId() + ") ");
            if (a.isPopular()) {
                System.out.print("*");
            } else {
                System.out.print(" ");
            }
            System.out.println(a.getSurname());
        }
    }
}