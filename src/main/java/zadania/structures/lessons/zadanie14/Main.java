package zadania.structures.lessons.zadanie14;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Zarzadca zarzadca = new Zarzadca();
        zarzadca.dodajMagazyn("Pierwszy magazyn", pobierzMagazynZProduktami("1"));
        zarzadca.dodajMagazyn("Pierwszy magazyn", pobierzMagazynZProduktami("2"));
        zarzadca.dodajMagazyn("Drugi magazyn", pobierzMagazynZProduktami("3"));
        zarzadca.dodajMagazyn("Trzeci magazyn", pobierzMagazynZProduktami("4"));
        zarzadca.dodajMagazyn("Czwarty magazyn", pobierzMagazynZProduktami("5"));
        zarzadca.dodajMagazyn("Piąty magazyn", pobierzMagazynZProduktami("6"));

        System.out.println("Przed czyszczeniem: ");
        System.out.println(zarzadca);
        zarzadca.wyczyscMagazyn();

        System.out.println();
        System.out.println("Po czyszczeniu: ");
        System.out.println(zarzadca);

        System.out.println();
        System.out.println("Najdroższe: ");
        System.out.println(zarzadca.pobierzNajdrozsze());
    }

    private static List<Produkt> generujProdukty(String label) {
        List<Produkt> produkts = new ArrayList<>();
        Random rand = new Random();
        for (int i = 0; i < 10; i++) {
            produkts.add(new Produkt(label + "_Produkt " + i, rand.nextInt(100), Kategoria.of(rand.nextInt() % Kategoria.values().length), LocalDate.of(rand.nextInt(20) + 2010, rand.nextInt(11) + 1, rand.nextInt(24) + 1)));
        }

        return produkts;
    }

    private static Magazyn pobierzMagazynZProduktami(String label) {
        Magazyn magazyn = new Magazyn();
        generujProdukty(label).forEach(magazyn::dodajProdukt);
        return magazyn;
    }
}
