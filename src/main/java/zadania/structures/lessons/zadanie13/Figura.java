package zadania.structures.lessons.zadanie13;

//Przygotuj interfejs Figura
//interface Figura:
//        getField();
//        getName();
public interface Figura {
    double getField();
    String getName();
}
