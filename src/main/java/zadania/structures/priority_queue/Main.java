package zadania.structures.priority_queue;

import lombok.AllArgsConstructor;
import lombok.ToString;

import java.util.PriorityQueue;

//Utwórz klasę Wiadomosc:
//class Wiadomosc:
//        id
//        tekst
//        priorytet
//
//        Następnie pobierz od użytkownika kilka wiadomości. Dodaj je do dwóch priorytetowych kolejek.
//        Jedna dodaje elementy wedlug dlugosci tekstu, druga wedlug priorytetu.
//
//        Wyświetl obie kolejki.
public class Main {
    @AllArgsConstructor
    @ToString
    static class Element {
        String name;
        int priorytet;
    }
    @AllArgsConstructor
    static class Wiadomosc {
        int id;
        String tekst;
        int priorytet;
    }
    public static void main(String[] args) {
//        PriorityQueue<Element> queue = new PriorityQueue<>(
//                (Element e1, Element e2) -> {
//                    System.out.println(e1.name + ": " + e1.name.length());
//                    System.out.println(e2.name + ": " + e2.name.length());
//                    return e2.name.length() - e1.name.length();
//                }
//        );
//
//        queue.add(new Element("a", 1));
//        queue.add(new Element("awdawdaw", 10));
//        queue.add(new Element("ala", 1));
//        queue.add(new Element("aa", 5));
//
//        while (queue.size() > 0) {
//            System.out.println(queue.poll());
//        }

        Wiadomosc wiadomosc1 = new Wiadomosc(1, "pierwszy", 5);
        Wiadomosc wiadomosc2 = new Wiadomosc(2, "drugi", 5);
        Wiadomosc wiadomosc3 = new Wiadomosc(3, "trzeci", 10);
        Wiadomosc wiadomosc4 = new Wiadomosc(4, "czwarty", 10);
        Wiadomosc wiadomosc5 = new Wiadomosc(5, "piąty", 10);

        PriorityQueue<Wiadomosc> queueByLength = new PriorityQueue<>((Wiadomosc w1, Wiadomosc w2) -> w1.tekst.length() - w2.tekst.length());
        queueByLength.add(wiadomosc1);
        queueByLength.add(wiadomosc2);
        queueByLength.add(wiadomosc3);
        queueByLength.add(wiadomosc4);
        queueByLength.add(wiadomosc5);


        PriorityQueue<Wiadomosc> queueByPriorytet = new PriorityQueue<>((Wiadomosc w1, Wiadomosc w2) -> w1.priorytet - w2.priorytet);
        queueByPriorytet.add(wiadomosc1);
        queueByPriorytet.add(wiadomosc2);
        queueByPriorytet.add(wiadomosc3);
        queueByPriorytet.add(wiadomosc4);
        queueByPriorytet.add(wiadomosc5);

        System.out.println("Kolejka po długości");
        while (queueByLength.size() > 0) {
            System.out.println(queueByLength.poll().tekst);
        }

        System.out.println();
        System.out.println("Kolejka po priorytecie");
        while (queueByPriorytet.size() > 0) {
            System.out.println(queueByPriorytet.poll().tekst);
        }
    }
}
