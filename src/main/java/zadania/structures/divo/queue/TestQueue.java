package zadania.structures.divo.queue;

public class TestQueue {
    public static void main(String[] args) {

        Queue<String> queue = new MyQueue<>();
        System.out.println(queue);

        queue.offer("test1");
        System.out.println(queue);
        queue.offer("test2");
        System.out.println(queue);
        queue.offer("test3");
        queue.offer("test4");
        queue.offer("test5");
        System.out.println(queue);

        String test = "";
        test = queue.poll();
        test = queue.poll();
        test = queue.poll();
        System.out.println(queue);

        queue.offer("test6");
        queue.offer("test7");
        queue.offer("test8");
        System.out.println(queue);

        test = queue.poll();
        queue.offer("test9");
        System.out.println(queue);
        test = queue.poll();
        test = queue.poll();
        test = queue.poll();
        test = queue.poll();
        queue.offer("test10");

        System.out.println(queue);
    }
}
