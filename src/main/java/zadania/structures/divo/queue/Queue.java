package zadania.structures.divo.queue;

public interface Queue<E> {

    /* Metoda ta zwraca wynik typu boolean informujący o statusie operacji. Może zdarzyć się, że kolejka nakłada pewne
     * ograniczenia, np. na maksymalną liczbę elementów, a więc dodanie nie zawsze musi się powieść (np. jeśli miałby
     * zostać przekroczony dopuszczalny rozmiar kolejki). Wartość true oznacza, że element został dodany a false, że z
     * jakichś względów nie było to możliwe. */
    boolean offer(E e);

    // Operacja poll() służy do pobrania elementu z kolejki. Jeśli kolejka jest pusta to operacja zwraca wartość null.
    // Pobierany element jest z kolejki usuwany.
    E poll();

    // Jeśli chcemy pobrać element bez usuwania go z kolejki, tj. element ten chcemy tylko podejrzeć, to używamy
    // operacji peek(). Zupełnie tak samo jak operacja pool() zwraca ona pobierany element lub null
    // jeśli kolejka była pusta
    E peek();

    void clear();
    int size();
    boolean isEmpty();
    String toString();
}
