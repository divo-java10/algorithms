package zadania.structures.divo.queue;

import java.util.LinkedList;
import java.util.List;

public class MyQueue<E> implements Queue<E> {
    private List<E> elements = new LinkedList<>();

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    @Override
    public boolean offer(E e) {
        return elements.add(e);
    }

    @Override
    public void clear() {
        elements.clear();
    }

    @Override
    public E poll() {
        return elements.remove(0);
    }

    @Override
    public E peek() {
        return elements.get(0);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        int i = 0;
        int size = elements.size();
        for (E s : elements) {
            sb.append(s);
            if (i++ < size - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
