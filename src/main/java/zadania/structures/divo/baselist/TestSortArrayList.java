package zadania.structures.divo.baselist;

public class TestSortArrayList extends TestArrayList {

    @Override
    public void add(Integer data) {
        add(indexOf(data), data);
    }

    public int indexOf(int data) {
        int index = 0;

        System.out.printf("index: %d, size: %d, data: %d\n", index, size(), data);
        for(int i = 0; i < size(); i++) {
            System.out.printf("  %d - %d\n", i, get(i));
            if (get(i) >= data) {
                break;
            }
            index = i + 1;
        }

        return index;
    }
}
