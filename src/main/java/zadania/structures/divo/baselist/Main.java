package zadania.structures.divo.baselist;

public class Main {
    public static void main(String[] args) {
        testMyArrayList();
        testMyLinkedList();
    }

    private static void testMyArrayList() {
        MyList<Integer> list = new MyArrayList<>();
//        ArrayList<Integer> list = new ArrayList<>();
//        TestArrayList list = new TestSortArrayList();
        list.add(10);
        list.add(30);
        list.add(60);
        list.add(20);
        list.add(40);
        list.add(50);

        System.out.print("Test array list:  ");
        System.out.println(list);
    }

    private static void testMyLinkedList() {
        MyList<String> list = new MyLinkedList<>();
        list.add("Test");
        list.add("Test1");
        list.add("Test2");

        list.add(0, "Test3");

        System.out.print("Test linked list: ");
        System.out.println(list);
    }
}
