package zadania.structures.divo.baselist;

public class TestArrayList implements MyList<Integer> {
    private final static int CAPACITY = 100;
    private Integer[] array;
    private int size;

    public TestArrayList() {
        array = new Integer[CAPACITY];
    }

    public TestArrayList(int rozmiar) {
        array = new Integer[rozmiar];
    }

    @Override
    public void add(Integer data) {
        add(size(), data);
    }

    @Override
    public void add(int index, Integer data) {
        if (array.length == size()) ensureCapacity(size() * 2 + 1);
        for (int i = size; i > index; i--) {
            array[i] = array[i - 1];
        }
        array[index] = data;
        size++;
    }

    private void ensureCapacity(int newSize) {
        Integer[] tempElements = new Integer[newSize];
        for (int i = 0; i < array.length; i++) {
            tempElements[i] = array[i];
        }
        array = tempElements;
    }

    public int size() {
        return size;
    }

    @Override
    public Integer get(int index) {
        if (index < 0) index = 0;
        if (index > size) index = size;

        return array[index];
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Integer e : array) {
            if (e == null) {
                continue;
            }
            sb.append(e);
            sb.append(", ");
        }
        sb.append("]");

        return sb.toString();
    }
}
