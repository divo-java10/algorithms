package zadania.structures.divo.baselist;

public class TestLinkedList implements MyList<Integer> {
    Node head = null;
    private int size = 0;

    @Override
    public void add(Integer e) {
        add(size, e);
    }

    @Override
    public void add(int index, Integer e) {
        if (head == null) {
            head = new Node();
            head.data = e;
            head.next = null;
        } else {
            Node n = getNode(index - 1);
            Node temp = new Node();
            temp.data = e;
            temp.next = n.next;
            n.next = temp;
        }
        size++;
    }

    @Override
    public Integer get(int index) {
        return getNode(index).data;
    }

    private Node getNode(int index) {
        Node n = head;
        int i = 0;
        while(true) {
            if (n == null || n.next == null) {
                return n;
            }

            if (i++ >= index) {
                return n;
            }

            n = n.next;
        }
    }

    private class Node {
        private int data;
        private Node next;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < size; i++) {
            sb.append(get(i));
            sb.append(",");
        }
        sb.append("]");

        return sb.toString();
    }
}
