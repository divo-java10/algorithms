package zadania.structures.divo.baselist;

public class MyLinkedList<T> implements MyList<T> {

    private static int counter;
    private Node<T> head;

    public void add(T data) {
        if (head == null) {
            head = new Node<>(data);
        }

        Node<T> tempNode = new Node<>(data);
        Node<T> currentNode = head;

        if (currentNode != null) {

            while (currentNode.getNext() != null) {
                currentNode = currentNode.getNext();
            }

            currentNode.setNext(tempNode);
        }

        counter++;
    }

    public void add(int index, T data) {
        if (head == null) {
            head = new Node<>(data);
        }

        Node<T> tempNode = new Node<>(data);
        Node<T> currentNode = head;

        int i = 0;
        if (currentNode != null) {
            while (currentNode.getNext() != null) {
                if (index == i++) {
                    break;
                }

                currentNode = currentNode.getNext();
            }

            tempNode.setNext(currentNode.getNext());
            currentNode.setNext(tempNode);
        }

        counter++;
    }

    public T get(int index) {
        if (index < 0)
            throw new IndexOutOfBoundsException("Index: " + index + ", Size " + counter );
        Node<T> currentNode;
        if (head != null) {
            currentNode = head.getNext();
            for (int i = 0; i < index; i++) {
                if (currentNode.getNext() == null)
                    return null;

                currentNode = currentNode.getNext();
            }
            return currentNode.getData();
        }
        return null;

    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        if (head != null) {
            Node<T> currentNode = head.getNext();
            while (currentNode != null) {
                sb.append(currentNode.getData().toString());
                if (currentNode.getNext() != null) {
                    sb.append(", ");
                }
                currentNode = currentNode.getNext();
            }
        }
        sb.append("]");

        return sb.toString();
    }

    private class Node<T> {
        Node<T> next;
        T data;

        public Node(T dataValue) {
            next = null;
            data = dataValue;
        }

        public Node(T dataValue, Node nextValue) {
            next = nextValue;
            data = dataValue;
        }

        public T getData() {
            return data;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node nextValue) {
            next = nextValue;
        }

    }
}
