package zadania.structures.divo.baselist;

public interface MyList<T> {

    void add(T data); // Dodaj na koniec tablicy
    void add(int index, T data); // dodaj w dowolnym miejscu tablicy
    T get(int index); // pobierz element o indexie
    String toString(); // pobierz string ze wszystkimi elementami w formacie
}
