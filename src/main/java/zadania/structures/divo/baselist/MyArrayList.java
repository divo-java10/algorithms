package zadania.structures.divo.baselist;

public class MyArrayList<E> implements MyList<E> {
    private int size = 0;
    private static final int DEFAULT_CAPACITY = 10;
    private Object[] elements;

    public MyArrayList() {
        elements = new Object[DEFAULT_CAPACITY];
    }

    public void add(E e) {
        add(size, e);
    }

    public void add(int index, E e) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size " + size );
        }
        if (index == elements.length || size == elements.length) {
            ensureCapa();
        }
        System.arraycopy(elements, index, elements, index + 1, size - index);
        elements[index++] = e;
        size++;
    }


    private void ensureCapa() {
        int newSize = elements.length * 2;
        Object[] tempElements = new Object[newSize];
        for (int i = 0; i < elements.length; i++) {
            tempElements[i] = elements[i];
        }
        elements = tempElements;
    }

    @SuppressWarnings("unchecked")
    public E get(int i) {
        if (i>= size || i <0) {
            throw new IndexOutOfBoundsException("Index: " + i + ", Size " + size );
        }
        return (E) elements[i];
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < size; i++) {
            sb.append(elements[i]);
            if (i < size - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");

        return sb.toString();
    }
}
