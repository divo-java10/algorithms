package zadania.structures.divo.hashmap;

import java.util.Objects;

public class Hashing {
    private int value1;
    private String value2;
    private boolean value3;
    private boolean value4;
    private char value5;

    // Cała idea HashMap opiera się na hashowaniu, co jest niczym więcej niż pewnym algorytmem. Działa on w ten sposób, że gdy podamy jakąś wartość zostanie zwrócony unikalny identyfikator reprezentujący ten obiekt. Ten unikalny integer nazywany jest `hash code`. Obowiązaują dwie zasady:
    // - jeżeli hashujemy ten sam obiekt zawsze dostaniemy ten sam hash
    // - dwa różne obiekty mogą mieć ten sam hash code.
    // Kiedy używamy metody put, HashMap nie dodaje na ślepo to wewnętrznej tablicy elementu. Zamiast tego wywołuje funkcję hashującą, a następnie dodaje element w odpowiednie miejsce tablicy. Poniżej jedna z implementacji funkcji hashCode():

    public int hashCode() {
        return Objects.hash(value1, value2, value3, value4, value5);
    }
}
