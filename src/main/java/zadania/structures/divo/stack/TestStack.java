package zadania.structures.divo.stack;

public class TestStack {
    public static void main(String[] args) {
        MyStack<String> stack = new MyStack<>();
        System.out.println(stack);

        stack.push("test1");
        System.out.println(stack);
        stack.push("test2");
        System.out.println(stack);
        stack.push("test3");
        stack.push("test4");
        stack.push("test5");
        System.out.println(stack);

        String test = "";
        test = stack.pop();
        test = stack.pop();
        test = stack.pop();
        System.out.println(stack);

        stack.push("test6");
        stack.push("test7");
        stack.push("test8");
        System.out.println(stack);

        test = stack.pop();
        stack.push("test9");
        System.out.println(stack);
        test = stack.pop();
        test = stack.pop();
        test = stack.pop();
        test = stack.pop();
        stack.push("test10");

        System.out.println(stack);
    }
}
