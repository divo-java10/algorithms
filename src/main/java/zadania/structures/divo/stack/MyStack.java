package zadania.structures.divo.stack;

import java.util.ArrayList;
import java.util.List;

public class MyStack<T> {
    private List<T> list = new ArrayList<>();

    public void push(T element) {
        list.add(element);
    }

    public T pop() {
        if (list.size() - 1 < 0) {
            return null;
        }

        T element = list.get(list.size() - 1);
        list.remove(list.size() - 1);

        return element;
    }

    public T get() {
        if (list.size() - 1 < 0) {
            return null;
        }

        T element = list.get(list.size() - 1);

        return element;
    }

    public MyStack<T> clone() {
        MyStack<T> stack = new MyStack<>();
        for (T e : list) {
            stack.push(e);
        }

        return stack;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        int i = 0;
        int size = list.size();
        for (T s : list) {
            sb.append(s);
            if (i++ < size - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyStack<?> stack = (MyStack<?>) o;

        return list != null ? list.equals(stack.list) : stack.list == null;
    }

    @Override
    public int hashCode() {
        return list != null ? list.hashCode() : 0;
    }
}
