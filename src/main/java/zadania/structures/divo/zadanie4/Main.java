package zadania.structures.divo.zadanie4;

//Napisz program zamieniający liczby naturalne podawane z klawiatury w postaci dziesiętnej na liczby zapisane w systemie rzymskim. Warunkiem zakończenia programu powinno by podanie liczby 0

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

class NumberConverter {
    private static final List<Integer> NUMBER_VALUES = Arrays.asList(1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1);
    private static final List<String> NUMBER_LETTERS = Arrays.asList("M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I");

    public static String toRoman(int num) {
        StringBuilder roman = new StringBuilder(); // Declare a string to hold the numerals
        for (int i = 0; i < NUMBER_VALUES.size(); i++) { // loop through all the values
            while (num >= NUMBER_VALUES.get(i)) { // Check if the number is greater than the current value
                roman.append(NUMBER_LETTERS.get(i)); // Add the letter to the String
                num -= NUMBER_VALUES.get(i); // Subtract the amount from the value
            }
        }
        return roman.toString(); // Return the String
    }
}

public class Main {
    public static void main(String[] args) {
        System.out.print("Wpisz liczbę w postaci dziesiętnej: ");
        int number = new Scanner(System.in).nextInt();

        System.out.println("Liczba w formacie rzymskim: " + NumberConverter.toRoman(number));
    }
}

