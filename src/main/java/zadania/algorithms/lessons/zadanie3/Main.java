package zadania.algorithms.lessons.zadanie3;

// 1.  Algorytm ma obliczać długość boku kwadratu o polu P.
// 1a. Pobierz od użytkownika pole kwadratu
// 1b. Wykorzystując metodę Math.sqrt oblicz bok kwadratu - pole: a^2

// 2. Pole trójkąta równobocznego: P = a^2 * sqrt(3) / 4
// 3. Pole koła: pi * r^2

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Scanner;
import java.util.stream.Stream;

public class Main {
    private double getSquareSide(int field) {
        return Math.sqrt(field);
    }
    private double getTriangleSide(int field) {
        return Math.sqrt(4 * field / Math.sqrt(3));
    }
    private double getRadius(int field) {
        return Math.sqrt(field / Math.PI);
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Main m = new Main();

        System.out.print("Podaj pole: ");
        int field = s.nextInt();

        System.out.println("Podaj funkcję, którą chcesz użyć");
        System.out.println("1. Kwadrat");
        System.out.println("2. Trójkąt");
        System.out.println("3. Koło");

        int choice = s.nextInt();
        Choice c = Choice.of(choice);

        switch(c) {
            case SQUARE:
                System.out.println("Bok kwadratu wynosi: " + m.getSquareSide(field));
                break;
            case TRIANGLE:
                System.out.println("Bok trójkąta wynosi: " + m.getTriangleSide(field));
                break;
            case CIRCLE:
                System.out.println("Promień koła wynosi: " + m.getRadius(field));
                break;
            case QUIT:
                break;
        }
    }

    @Getter
    @AllArgsConstructor
    enum Choice {
        SQUARE(1), TRIANGLE(2), CIRCLE(3), QUIT(-1);

        private final int choice;

        public static Choice of(int c) {
            return Stream.of(values())
                    .filter(ch -> ch.getChoice() == c)
                    .findAny()
                    .get();
        }
    }
}
