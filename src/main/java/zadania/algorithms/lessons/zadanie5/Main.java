package zadania.algorithms.lessons.zadanie5;

public class Main {
    private void palindrom1(int n, int m) {
        int k = n + m;
        for (int i = n; i <= k; i++) {
            System.out.print(i);
            System.out.print(", ");
        }
        for (int i = k - 1; i >= n; i--) {
            System.out.print(i);
            System.out.print(", ");
        }
    }
    private void palindrom2(int n, int m) {
        int k = n + (m * 2);
        int l = n + m;
        for (int i = n; i <= k; i++) {
            System.out.printf("%d, ", (i > l) ? l * 2 - i : i);
        }
    }
    public boolean isPalindrom(char[] word){
        int i1 = 0;
        int i2 = word.length - 1;
        while (i2 > i1) {
            if (word[i1] != word[i2]) {
                return false;
            }
            ++i1;
            --i2;
        }

        return true;
    }

    public static void main(String[] args) {
        int n = 10;
        int m = 5;
        String palindrom = "mozejutrotadamadatortujezom";
        String notPalindrom = "mozejutrotaadamadatortujezom";

        Main main = new Main();
        System.out.println("--- Palindrom 1");
        main.palindrom1(n, m);
        System.out.println("\n--- Palindrom 2");
        main.palindrom2(n, m);
        System.out.println("\n--- Czy palindrom1: " + main.isPalindrom(palindrom.toCharArray()));
        System.out.println("--- Czy palindrom2: " + main.isPalindrom(notPalindrom.toCharArray()));
    }
}
