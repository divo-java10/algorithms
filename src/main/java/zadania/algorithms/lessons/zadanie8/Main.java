package zadania.algorithms.lessons.zadanie8;

import lombok.Builder;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

// Podstawa:
// Pobierz listę napisów
// Dla każdego napisu oblicz sumę znaków.
// Sumę znaków obliczamy na podstawie:
// Ustaw zmienną pomocniczą, która przechowa sumę wszystkich znaków
// Dla każdej litery w słowie:
// Pobierz numer litery (pobierz kod ascii i odejmij: 96 - dla małych liter, 65 - dla wielkich)
// dodaj do sumy numer litery
// zwróć sumę.
// Wyświetl słowo oraz sumę liter.

@Getter
@Builder
class Summary {
    private final List<Integer> codes;
    private final int sum;
    private final String word;

    public String toString() {
        StringBuilder sb = new StringBuilder(word);
        sb.append(" = ");
        sb.append(codes.stream()
                .map(Object::toString)
                .collect( Collectors.joining( "," )));
        sb.append(" = ");
        sb.append(sum);

        return sb.toString();
    }
}

public class Main {
    private static final Map<Character, Integer> weights = new HashMap<>();
    static {
        weights.put('a', 60);
        weights.put('b', 7);
        weights.put('c', 20);
        weights.put('d', 50);
        weights.put('e', 15);
        weights.put('f', 8);
        weights.put('g', 11);
        weights.put('h', 11);
    }
    private Summary summarizeLetters(String word, int start, Map<Character, Integer> weights) {
        List<Integer> codes = new ArrayList<>();
        int sum = 0;
        for (int i = 0; i < word.length(); i++) {
            Character actualLetter = word.charAt(i);
            int code = word.codePointAt(i) - start;
            if (weights.containsKey(actualLetter)) {
                code *= weights.get(actualLetter);
            }
            sum += code;
            codes.add(code);
        }

        return Summary.builder()
                .codes(codes)
                .sum(sum)
                .word(word)
                .build();
    }
    private List<Summary> summarizeSmallLetters(List<String> words) {
        List<Summary> summaries = new ArrayList<>();

        for (String word : words) {
            summaries.add(summarizeLetters(word.toLowerCase(), 96, Collections.emptyMap()));
        }

        return summaries;
    }
    private List<Summary> summarizeLettersWithBig(List<String> words) {
        List<Summary> summaries = new ArrayList<>();

        for (String word : words) {
            summaries.add(summarizeLetters(word, 65, Collections.emptyMap()));
        }

        return summaries;
    }
    private List<Summary> summarizeSmallLettersAndWeights(List<String> words) {
        List<Summary> summaries = new ArrayList<>();

        for (String word : words) {
            summaries.add(summarizeLetters(word.toLowerCase(), 96, weights));
        }

        return summaries;
    }
    private int levenshtein(String s, String t) {
        int i, j, m, n, cost;
        int d[][];

        m = s.length();
        n = t.length();

        d = new int[m+1][n+1];

        for (i=0; i<=m; i++) {
            d[i][0] = i;
        }
        for (j=1; j<=n; j++) {
            d[0][j] = j;
        }

        for (i=1; i<=m; i++) {
            for (j=1; j<=n; j++) {
                if (s.charAt(i-1) == t.charAt(j-1)) {
                    cost = 0;
                } else {
                    cost = 1;
                }

                d[i][j] = Math.min(d[i-1][j] + 1,         /* remove */
                        Math.min(d[i][j-1] + 1,      /* insert */
                                d[i-1][j-1] + cost));   /* change */
            }
        }

        return d[m][n];
    }

    public static void main(String[] args) {
        Main main = new Main();
        List<String> words = Arrays.asList("Ala", "Ola", "pies", "Marek", "Arek");

        System.out.println("Summarize with only small letters");
        List<Summary> summaries = main.summarizeSmallLetters(words);
        summaries.sort(Comparator.comparingInt(Summary::getSum).reversed());
        summaries.forEach(System.out::println);

        System.out.println("\nSummarize with big letters");
        summaries = main.summarizeLettersWithBig(words);
        summaries.sort(Comparator.comparingInt(Summary::getSum).reversed());
        summaries.forEach(System.out::println);

        System.out.println("\nSummarize with only small letters and weights");
        summaries = main.summarizeSmallLettersAndWeights(words);
        summaries.sort(Comparator.comparingInt(Summary::getSum).reversed());
        summaries.forEach(System.out::println);

        System.out.println("\nAlgorytm Levenshteina");
        for (String word : words) {
            int levenshtein = main.levenshtein(words.get(0), word);
            double ratio = 1 - ((double) levenshtein) / (Math.max(words.get(0).length(), word.length()));
            System.out.println("Word: " + word + ", minimal: " + levenshtein + ", probability: " + ratio);
        }
    }
}
