package zadania.algorithms.lessons.zadanie1;

// 1. Przedstaw opis słowny problemu
// 1a. Dane są dwie liczby a i b;
// 1b. Jeśli a jest równe b, to NWD jest równe a,
// 1c. W przeciwnym wypadku, jeżeli a jest większe od b, to zmień a na równe a - b, a jeżeli a jest mniejsze od b to zmień b na b - a;
// 1d. Zacznij od początku.

//założenie a > b
//podprogram NWD(a, b)
//        dopóki b ≠ 0
//        c := reszta z dzielenia a przez b
//        a := b
//        b := c
//        zwróć a

public class Main {
    public int NWD(int a, int b) {
        while (b != 0) {
            int c = a % b;
            a = b;
            b = c;
        }

        return a;
    }
    public static void main(String[] args) {
        Main m = new Main();
        System.out.println(m.NWD(15, 3));
    }
}