package zadania.algorithms.lessons.zadanie2;

// 1.  Wykorzystując TYLKO dodawanie pomnożyć pierwszą liczbę przez drugą.
// 1a. Pobierz od użytkownika wartość zmiennej a
// 1b. Pobierz od użytkownika wartość zmiennej b
// 1c. Dopóki nie osiągnęliśmy wartości a dodawaj b i przypisz do zmiennej suma
// 1d. Wyświetl sumę

// 2.  Wykorzystując TYLKO dodawanie spotęgować pierwszą liczbę przez drugą.
// 2a. Pobierz od użytkownika wartość zmiennej a
// 2b. Pobierz od użytkownika wartość zmiennej b
// 2c. Przypisz do zmiennej pomocniczej liczbę b
// 2d. Dopóki nie osiągnęliśmy wartości b dodawaj zmienną pomocniczą
// 2e. Dopóki nie osiągnęliśmy wartości a wykonuj krok c oraz d
// 2f. Wyświetl zmienną pomocniczą.

public class Main {
    private int multiply(int a, int b) {
        int suma = 0;
        for (int i = 0; i < b; i++) {
            suma += a;
        }

        return suma;
    }

    public static void main(String[] args) {
        Main m = new Main();
        System.out.println(m.multiply(6, 8));
        System.out.println(m.multiply(6, 2));
        System.out.println(m.multiply(6, 4));
        System.out.println(m.multiply(2, 6));

        int podstawa = 2;
        int wykładnik = 4;

        int wynik = podstawa;
        for (int i = 1; i < wykładnik; i++) {
            wynik = m.multiply(wynik, podstawa);
        }

        System.out.println(wynik);
    }
}
