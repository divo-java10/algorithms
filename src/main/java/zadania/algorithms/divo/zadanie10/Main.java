package zadania.algorithms.divo.zadanie10;

class VigenerCipher {
    private char[][] alphabetTable = new char['z' + 1]['z' + 1];

    VigenerCipher() {
        for (char a = 'a'; a <= 'z'; a++) {
            char b = a;
            for (int z = 'a'; z <= 'z'; z++) {
                if (b == 'z' + 1) {
                    b = 'a';
                }
                alphabetTable[a][z] = b;
                b++;
            }
        }
    }

    private String padKey(String inputString, String passString) {
        String key = passString;
        for (; key.length() < inputString.length();) {
            key += key;
        }

        return key;
    }

    /**
     * Szyfruj dane
     */
    public String crypt(String inputString, String passString) {
        char[] textCharTable = inputString.toCharArray();
        char[] passCharTable = padKey(inputString, passString).toCharArray();

        StringBuilder sb = new StringBuilder();
        for (int index = 0; index < textCharTable.length; index++) {
            if (textCharTable[index] != ' ') {
                sb.append(alphabetTable[textCharTable[index]][passCharTable[index]]);
            } else {
                sb.append(" ");
            }
        }

        return sb.toString();
    }

    /**
     * Deszyfruj dane
     */
    public String decrypt(String inputString, String passString) {
        char[] textCharTable = inputString.toCharArray();
        char[] passCharTable = padKey(inputString, passString).toCharArray();

        StringBuilder sb = new StringBuilder();
        for (int index = 0; index < textCharTable.length; index++) {
            if (textCharTable[index] != ' ') {
                for (int z = 'a'; z <= 'z'; z++) {
                    if (textCharTable[index] == alphabetTable[passCharTable[index]][z]) {
                        sb.append(alphabetTable['a'][z]);

                    }
                }
            } else {
                sb.append(" ");
            }
        }

        return sb.toString();
    }
}

public class Main {
    public static void main(String[] args) {
        String word = "tekst do zaszyfrowania";
        String key = "n";

        VigenerCipher cipher = new VigenerCipher();
        String encrypted = cipher.crypt(word, key);
        String decrypted = cipher.decrypt(encrypted, key);

        System.out.println("Original word: " + word);
        System.out.println("Encrypted by Vigener: " + encrypted);
        System.out.println("Decrypted by Vigener: " + decrypted);
        System.out.println("Are same: " + decrypted.equals(word));
    }
}
