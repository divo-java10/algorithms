package zadania.algorithms.divo.zadanie6;

// 1.  Napisz funkcję, która narysuje na ekranie pełny prostokąt
// 1a. Wyświetl "_" dopóki nie osiągnie szerokości
// 1b. Powtórz a dopóki nie osiągnie wysokości

public class Main {
    private void rectangle1(int height, int width) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print("# ");
            }

            System.out.println();
        }
    }
    private void rectangle2(int height, int width) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (i == height - 1 || j == width - 1 || i == 0 || j == 0) {
                    System.out.print("# ");
                } else {
                    System.out.print("  ");
                }
            }

            System.out.println();
        }
    }

    private void triangle1(int height) {
        for (int i = 1; i <= height; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print('*');
            }

            System.out.println();
        }
    }
    private void triangle2(int height) {
        for (int i = 1; i <= height; i++) {
            for (int j = 0; j < i; j++) {
                if (j == 0 || j == i - 1 || i == 1 || i == height) {
                    System.out.print('*');
                } else {
                    System.out.print(' ');
                }
            }

            System.out.println();
        }
    }
    private void circle1(int posY, int posX, int radius) {
        for (int i = 0;i <= posX + radius; i++) {
            for (int j = 0;j <=posY + radius; j++) {
                int xSquared = (i - posX)*(i - posX);
                int ySquared = (j - posY)*(j - posY);
                if (Math.abs(xSquared + ySquared - radius) <= radius) {
                    System.out.print("# ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
    private void circle2(int posY, int posX, int radius) {
        for (int i = 0;i <= posX + radius ; i++) {
            for (int j = 0;j <=posY + radius; j++) {
                int xSquared = (i - posX)*(i - posX);
                int ySquared = (j - posY)*(j - posY);
                if (Math.abs(xSquared + ySquared - radius * radius) < radius) {
                    System.out.print("# ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Main m = new Main();
        m.rectangle1(5, 10);
        System.out.println();
        m.rectangle2(5, 10);
        System.out.println();
        m.triangle1(5);
        System.out.println();
        m.triangle2(5);
        System.out.println();
        m.circle1(5, 5, 5);
        System.out.println();
        m.circle2(5, 5, 5);
    }
}
